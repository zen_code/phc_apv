

import React, { Component } from 'react';
import { Root } from './src/routers/mainRouter';

export default class App extends Component {
  render() {
    return (
        <Root />
    );
  }
}
