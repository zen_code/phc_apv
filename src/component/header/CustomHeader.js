import React, { Component } from "react";
import {
  Platform,
  Image,
  AsyncStorage,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import {
  Footer,
  FooterTab,
  Text,
  Button,
  Icon,
  View,
  Left,
  Body,
  Title,
  Right,
  Header} from "native-base";
import {RFValue} from "react-native-responsive-fontsize";
import IconMI from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityicons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from "../styles/colors";
import MyView from "../view/MyView";

class CustomHeader extends Component {
  constructor(props) {
    super(props);
    this.state={

    }
  }

  componentDidMount() {

  }

  render() {
    return (
        <View>
          <Header style={styles.header}>
            <Left style={{ flex: 1}}>
              <MyView hide={!this.props.left}>
                <TouchableOpacity
                    transparent
                    onPress={()=>this.props.navigation.goBack()}
                >
                  <Icon
                      name='arrow-back'
                      style={styles.iconLeft}
                  />
                </TouchableOpacity>
              </MyView>
            </Left>
            <Body style={{ flex:3, alignItems:'center'}}>
              <Text style={styles.title}>{this.props.title}</Text>
            </Body>
            <Right style={{ flex: 1}}>
              <MyView hide={!this.props.right}>
                <TouchableOpacity
                    transparent
                    onPress={()=>this.props.navigation.goBack()}
                >
                  <Icon
                      name='notifications-outline'
                      style={styles.iconRight}
                  />
                </TouchableOpacity>
              </MyView>
            </Right>
          </Header>
        </View>
    );
  }
}

export default CustomHeader;

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.white_1st
  },
  iconLeft: {
    color:colors.black_1st,
    fontSize:RFValue(19),
    paddingLeft:10
  },
  iconRight: {
    color:colors.black_1st,
    fontSize:RFValue(25),
    marginLeft:RFValue(-20)
  },
  title: {
    fontSize:RFValue(17),
    color:colors.black_1st,
    fontFamily: 'Montserrat-Regular',
  }
});

