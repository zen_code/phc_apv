import React, { Component } from "react";
import {Platform, Image, AsyncStorage, StyleSheet} from "react-native";
import { Footer, FooterTab, Text, Button, Icon, View, Badge } from "native-base";
import {RFValue} from "react-native-responsive-fontsize";
import colors from "../styles/colors/."
import IconMC from "react-native-vector-icons/MaterialCommunityIcons";

class CustomFooter extends Component {
  constructor(props) {
    super(props);
    this.state={
      notif:0
    }
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      // this.loadNotif();
    });
    //this.loadData();

  }

  render() {

    return (
        <View>
          <Footer>
            <FooterTab style={{ backgroundColor: colors.white_2st}}>
              <Button
                  vertical
                  onPress={() => this.props.navigation.navigate("Dashboard")}
              >
                <IconMC
                    name={this.props.tab == "Dashboard" ? 'home' : 'home-outline'}
                    size={RFValue(22)}
                    color={this.props.tab == "Dashboard" ? colors.blue_2st : colors.gray_1st}
                />
                <Text style={[{color: this.props.tab == "Dashboard" ? colors.blue_2st : colors.gray_1st},styles.title]}>Dashboard</Text>
              </Button>
              <Button
                  vertical
                  onPress={() => this.props.navigation.navigate("History")}
              >
                <IconMC
                    name={this.props.tab == "History" ? 'history' : 'history'}
                    size={RFValue(22)}
                    color={this.props.tab == "History" ? colors.blue_2st : colors.gray_1st}
                />
                <Text style={[{color: this.props.tab == "History" ? colors.blue_2st : colors.gray_1st, fontSize:RFValue(10)},styles.title]}>History</Text>
              </Button>
              <Button
                  vertical
                  onPress={() => this.props.navigation.navigate("Profile")}
              >
                <IconMC
                    name={this.props.tab == "Profile" ? 'account' : 'account-outline'}
                    size={RFValue(22)}
                    color={this.props.tab == "Profile" ? colors.blue_2st : colors.gray_1st}
                />
                <Text style={[{color: this.props.tab == "Profile" ? colors.blue_2st : colors.gray_1st, fontSize:RFValue(10)},styles.title]}>Profile</Text>
              </Button>

            </FooterTab>
          </Footer>
        </View>
    );
  }
}

export default CustomFooter;

const styles = StyleSheet.create({
  icon: {
    width:RFValue(25),
    height:RFValue(25),
  },
  icon2: {
    width:RFValue(23),
    height:RFValue(23),
  },
  title:{
    fontSize:RFValue(10),
    fontFamily: 'Montserrat-Regular',
  }
})
