export default {
  white_1st : '#FFFFFF',
  white_2st : '#F7F9FB',
  white_3st : '#F2F2F2',
  gray_1st : '#A8A8A8',
  gray_2st : '#808080',
  blue_1st : '#e3f5fa',
  blue_2st : "#4179e8",
  blue_3st : "#118EEA",
  blue_4st : "#428DFF",
  green_1st : "#23ad58",

  red_1st : '#C70D3A',
  black_1st : '#000000',
  orange_1st : '#FF8C00',
};
