import React, { Component } from "react";
import {View, Text, Image, TouchableNativeFeedback, StyleSheet} from "react-native";
import { Icon } from "native-base";
import colors from "../styles/colors/index";
import PropTypes from "prop-types";
import {RFValue} from "react-native-responsive-fontsize";

export default class SubHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { loading, disabled, handleOnPress } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.cardTop}>
                    <Text style={styles.title} numberOfLines={2}>{this.props.title}</Text>
                    <Text style={styles.dokter} numberOfLines={2}>{this.props.dokter}</Text>
                </View>
                <View style={styles.cardBottom}>
                    <View style={[styles.btnStatus,{backgroundColor: this.props.status == 'Approved'? colors.green_1st : colors.red_1st}]}>
                        <Text style={styles.status} numberOfLines={1}>{this.props.status}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 0,
        marginRight: 0,
        marginBottom: RFValue(10),
        flexDirection:'column',
        backgroundColor:colors.white_1st,
        borderRadius:RFValue(5),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 1,
        padding:RFValue(10)

    },
    icon: {
        width: RFValue(40),
        height: RFValue(40),
    },
    cardTop: {

    },
    cardBottom: {
        borderRadius:RFValue(5),
        alignItems: 'flex-end',
    },
    title: {
        fontSize:RFValue(12),
        color:colors.black_1st,
        fontFamily: 'Montserrat-Regular',
    },
    dokter: {
        fontSize:RFValue(13),
        color:colors.gray_1st,
        fontFamily: 'Montserrat-LightItalic',
    },
    status: {
        fontSize:RFValue(12),
        color:colors.white_1st,
        fontFamily: 'Montserrat-Regular',
    },
    btnStatus: {
        paddingLeft: RFValue(5),
        paddingRight: RFValue(5),
        paddingTop: RFValue(2),
        paddingBottom: RFValue(2),
        borderRadius:RFValue(7)
    }
})


SubHistory.propTypes = {
    handleOnPress: PropTypes.func,
    disabled: PropTypes.bool
};



