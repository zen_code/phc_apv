import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    Modal,
    Image,
    TouchableOpacity
} from 'react-native';
import {RFValue} from "react-native-responsive-fontsize";

const Loader = props => {
    const {
        loading,
        title,
        ...attributes
    } = props;


    this.state = {
        isVisible: false
    }

    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={loading ? loading : this.state.isVisible}
            onRequestClose={() => { console.log('close modal') }}
        >
            <View style={styles.modalBackground}>
                <View style={{ width: RFValue(40), height: RFValue(40), backgroundColor: '#fff', borderRadius: 50 }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            style={{ width: RFValue(30), height: RFValue(30)}}
                            source={require('./loader.gif')}
                        />
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000090'
    },
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});
export default Loader;
