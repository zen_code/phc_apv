import React from 'react';
import {
    createAppContainer,
    createSwitchNavigator,
    createBottomTabNavigator,
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SplashScreen from "../screens/auth/SplashScreen";
import Login from "../screens/auth/Login";
import Dashboard from "../screens/dashboard/Dashboard";
import Approval from "../screens/approval/Approval";
import DetailApproval from "../screens/approval/DetailApproval";
import History from "../screens/history/History";
import Profile from "../screens/auth/Profile";

export const All = createStackNavigator({
    SplashScreen: {
        screen: SplashScreen
    },
    Login: {
        screen: Login
    },
    Dashboard: {
        screen: Dashboard
    },
    Approval: {
        screen: Approval
    },
    DetailApproval: {
        screen: DetailApproval
    },
    History: {
        screen: History
    },
    Profile: {
        screen: Profile
    }
});

export const Root = createAppContainer(All)
