import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    ActivityIndicator,
    AsyncStorage,
    FlatList,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    Alert,
    Platform,
    TextInput,
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Card,
    CardItem,
    Textarea
} from "native-base";
import MaterialCommunityicons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
// import HomeMenu from "../../component/HomeMenu";
// import TopMenu from "../../component/TopMenu";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CustomHeader from "../../component/header/CustomHeader";
// import CustomFooter from "../../component/footer/CustomFooter";

import SubApproval from "../../component/card/SubApproval";
import LinearGradient from 'react-native-linear-gradient';
import IconMI from "react-native-vector-icons/MaterialIcons";
import Loader from "../../component/loader/loader";
import colors from "../../component/styles/colors/index";
import {RFValue} from "react-native-responsive-fontsize";
import size from "../../component/styles/size/index";

var that;

class ListApproval extends React.PureComponent {
    render() {
        return (
            <View style={{marginLeft:RFValue(10), marginRight:RFValue(10)}}>
                <TouchableOpacity
                    transparent
                    onPress={() => this.navigateToScreen("DetailTentor", this.props.data)}>
                    <SubApproval
                        title={this.props.data.title}
                        dokter={this.props.data.dokter}
                        time={this.props.data.time}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

export default class DetailApproval extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listApproval :[
                {
                    'title' : 'Pembatalan Jadwal Operasi Bedah Mata',
                    'dokter' : 'dr suzana',
                    'time' : 30
                },
                {
                    'title' : 'Pembatalan',
                    'dokter' : 'dr suzana',
                    'time' : 30
                },
                {
                    'title' : 'Pembatalan Jadwal Operasi Bedah Mata',
                    'dokter' : 'dr suzana',
                    'time' : 30
                },
                {
                    'title' : 'Pembatalan',
                    'dokter' : 'dr suzana',
                    'time' : 30
                },
                {
                    'title' : 'Pembatalan Jadwal Operasi Bedah Mata',
                    'dokter' : 'dr suzana',
                    'time' : 30
                },
                {
                    'title' : 'Pembatalan',
                    'dokter' : 'dr suzana',
                    'time' : 30
                }
            ]
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {

    }

    _renderApproval = ({ item, index }) => <ListApproval data={item} index={index}/>;

    render() {

        that=this;
        return (
            <Container style={styles.container}>
                <Loader loading={this.state.loading} />
                <CustomHeader navigation={this.props.navigation} title="Detail Approval" left={true} right={false}/>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.approval}>
                        <Text style={styles.title}>Penggantian tanggal KRS dan MRS</Text>
                        <Text style={styles.detail}>Penggantian tanggal KRS dan MRS</Text>
                    </View>
                </ScrollView>

                <Footer>
                    <FooterTab style={styles.contentFooter}>
                        <View style={styles.cententButton}>
                            <View style={styles.btnReject}>
                                <Text style={styles.titleButton}>REJECT</Text>
                            </View>
                        </View>
                        <View style={styles.cententButton}>
                            <View style={styles.btnApprove}>
                                <Text style={styles.titleButton}>APPROVE</Text>
                            </View>
                        </View>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        backgroundColor: colors.blue_3st,
        width:wp('100%'),
        height: RFValue(190),
        paddingLeft: RFValue(10),
        paddingRight: RFValue(10),
        paddingTop:RFValue(25),
        flexDirection:'row',
    },
    container: {
        flex: 1,
        backgroundColor: colors.white_2st
    },
    content:{
        flex:1,
        marginTop:5,
        height:RFValue(50),
    },
    approval:{
        paddingBottom:RFValue(10),
        marginTop:RFValue(20),
        paddingLeft:RFValue(20),
        paddingRight:RFValue(20)
    },
    icon:{
        width: RFValue(25),
        height: RFValue(25),
    },
    iconTop:{
        width: RFValue(40),
        height: RFValue(40),
    },
    title:{
        fontSize: RFValue(15),
        color:colors.black_1st,
        fontFamily: 'Montserrat-Bold',
    },
    detail: {
        fontSize: RFValue(12),
        marginTop:RFValue(5),
        color:colors.gray_1st,
        fontFamily: 'Montserrat-Regular',
    },
    contentFooter: {
        backgroundColor: colors.white_2st,
        flex: 1,
        flexDirection: 'row',
    },
    cententButton: {
        width:'50%',
        justifyContent:'center',
        alignItems:'center',
        paddingLeft:RFValue(10),
        paddingRight:RFValue(10),
        paddingTop:RFValue(5),
        paddingBottom:RFValue(5)
    },
    btnReject: {
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: colors.red_1st,
        width:'100%',
        height:'100%',
        borderRadius:RFValue(20)
    },
    btnApprove: {
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: colors.blue_3st,
        width:'100%',
        height:'100%',
        borderRadius:RFValue(20)
    },
    titleButton: {
        fontSize: RFValue(12),
        color:colors.white_1st,
        fontFamily: 'Montserrat-Regular',
    }


});
