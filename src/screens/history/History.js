import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    ActivityIndicator,
    AsyncStorage,
    FlatList,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    Alert,
    Platform,
    TextInput,
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Card,
    CardItem,
    Textarea
} from "native-base";
import MaterialCommunityicons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
// import HomeMenu from "../../component/HomeMenu";
// import TopMenu from "../../component/TopMenu";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CustomHeader from "../../component/header/CustomHeader";
import CustomFooter from "../../component/footer/CustomFooter";

import SubHistory from "../../component/card/SubHistory";
import LinearGradient from 'react-native-linear-gradient';
import IconMI from "react-native-vector-icons/MaterialIcons";
import Loader from "../../component/loader/loader";
import colors from "../../component/styles/colors/index";
import {RFValue} from "react-native-responsive-fontsize";
import size from "../../component/styles/size/index";

var that;

class ListHistory extends React.PureComponent {
    navigateToScreen() {
        // AsyncStorage.setItem("listModule", JSON.stringify(listModule)).then(() => {
        that.props.navigation.navigate("DetailApproval");
        // })
    }

    render() {
        return (
            <View style={{marginLeft:RFValue(10), marginRight:RFValue(10)}}>
                <TouchableOpacity
                    transparent
                    onPress={() => this.navigateToScreen()}>
                    <SubHistory
                        title={this.props.data.title}
                        dokter={this.props.data.dokter}
                        status={this.props.data.status}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

export default class History extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listHistory :[
                {
                    'title' : 'Pembatalan Jadwal Operasi Bedah Mata',
                    'dokter' : 'dr suzana',
                    'status' : 'Approved'
                },
                {
                    'title' : 'Pembatalan',
                    'dokter' : 'dr suzana',
                    'status' : 'Rejected'
                },
                {
                    'title' : 'Pembatalan Jadwal Operasi Bedah Mata',
                    'dokter' : 'dr suzana',
                    'status' : 'Expired'
                },
                {
                    'title' : 'Pembatalan',
                    'dokter' : 'dr suzana',
                    'status' : 'Expired'
                },
                {
                    'title' : 'Pembatalan Jadwal Operasi Bedah Mata',
                    'dokter' : 'dr suzana',
                    'status' : 'Rejected'
                },
                {
                    'title' : 'Pembatalan',
                    'dokter' : 'dr suzana',
                    'status' : 'Approved'
                }
            ]
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {

    }

    _renderHistory = ({ item, index }) => <ListHistory data={item} index={index}/>;

    render() {

        that=this;
        return (
            <Container style={styles.container}>
                <Loader loading={this.state.loading} />
                <CustomHeader navigation={this.props.navigation} title="History Approval" left={true} right={false}/>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.searchContent}>
                        <View style={{width:'90%', height:'100%', justifyContent:'center'}}>
                            <TextInput style={styles.searchApproval} rowSpan={1} value={this.state.searchTentor} placeholder='Cari History Rawat Inap' onChangeText={(text) => this.setState({ searchTentor: text })} />
                        </View>
                        <View style={{width:'15%', height:'100%', justifyContent:'center', alignItems:'center'}}>
                            <IconMI
                                name='search'
                                style={styles.iconSearch}
                            />
                        </View>
                    </View>

                    <View style={styles.history}>
                        <FlatList
                            data={this.state.listHistory}
                            renderItem={this._renderHistory}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>

                </ScrollView>
                <CustomFooter navigation={this.props.navigation} tab="History" />
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        backgroundColor: colors.blue_3st,
        width:wp('100%'),
        height: RFValue(190),
        paddingLeft: RFValue(10),
        paddingRight: RFValue(10),
        paddingTop:RFValue(25),
        flexDirection:'row',
    },
    name:{
        fontSize:RFValue(20),
        fontWeight:"bold",
        color:colors.white_1st,
        fontFamily: 'Montserrat-Regular',
    },
    balance:{
        fontSize:RFValue(16),
        color:colors.gray_1st,
        fontFamily: 'Montserrat-Regular',
    },
    rp:{
        fontSize:RFValue(16),
        textAlign:'right',
        fontFamily: 'Montserrat-Regular',
    },
    nominal:{
        fontSize:RFValue(12),
        fontFamily: 'Montserrat-Regular',
    },
    info:{
        fontSize:RFValue(13),
        color:colors.white_1st,
        marginTop:RFValue(10),
        fontFamily: 'Montserrat-Regular',
    },
    saldo:{
        flex:1,
        paddingTop: RFValue(15),
        paddingLeft:RFValue(5),
        paddingRight: RFValue(5),
    },
    container: {
        flex: 1,
        backgroundColor: colors.white_2st
    },
    content:{
        flex:1,
        marginTop:5,
        height:RFValue(50),
    },
    cardTop:{
        marginLeft: RFValue(10),
        marginRight: RFValue(10),
        marginTop:RFValue(-80),
        paddingTop:RFValue(30),
        paddingBottom:RFValue(30),
        borderRadius: 10,
        backgroundColor: colors.white_1st,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 1,
    },
    cardFitur:{
        flex:1,
        flexDirection:'row',
        marginTop:25,
        marginLeft: RFValue(15),
        marginRight: RFValue(15),
    },
    menu:{
        padding:2,
        flex: 1,
        marginHorizontal:10,
        alignItems: 'center',
        width:'25%',
        justifyContent:'flex-start',
        marginBottom:RFValue(20)
    },
    gradient:{
        height:'120%',
        width:'100%',
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center'
    },
    history:{
        paddingBottom:RFValue(10),
        marginTop:RFValue(20)
    },
    icon:{
        width: RFValue(25),
        height: RFValue(25),
    },
    iconTop:{
        width: RFValue(40),
        height: RFValue(40),
    },
    title:{
        marginTop:RFValue(15),
        fontSize: RFValue(11),
        color:colors.gray_1st,
        fontFamily: 'Montserrat-Bold',
    },
    titleMenu:{
        marginTop:RFValue(5),
        fontSize: RFValue(12),
        color:colors.black_1st,
        textAlign:'center',
        fontFamily: 'Montserrat-Regular',
    },
    profile:{
        width: RFValue(30),
        height: RFValue(30),
        borderRadius: RFValue(15),
        borderWidth:1,
        borderColor:colors.white_1st
    },
    listApproval: {
        margin:RFValue(15),
        fontSize:RFValue(13),
        fontFamily: 'Montserrat-Regular',
    },
    lihatLainnya: {
        fontSize:RFValue(13),
        margin:RFValue(15),
        color:colors.blue_2st,
        fontFamily: 'Montserrat-Regular',
    },
    searchContent: {
        backgroundColor:'#d2d2d2',
        borderRadius:20,
        height:RFValue(30),
        flex:1,
        flexDirection:'row',
        marginLeft:RFValue(10),
        marginRight:RFValue(10),
        marginTop:RFValue(10),
        paddingLeft:RFValue(10),
        paddingRight:RFValue(10)
    },
    searchApproval:{
        fontSize:RFValue(12),
        color:colors.black_1st,
        fontFamily: 'Montserrat-Regular',
    },
    iconSearch: {
        color:colors.black_1st,
        fontSize:RFValue(17),
    },
    // notifAwal: {
    //     width:RFValue(20),
    //     height:RFValue(20),
    //     backgroundColor:colors.green_1st,
    //     borderRadius:RFValue(10),
    //     marginTop:RFValue(10),
    //     // marginLeft:RFValue(45),
    //     justifyContent:'center',
    //     alignItems:'center'
    // },
    // notif: {
    //     width:RFValue(20),
    //     height:RFValue(20),
    //     backgroundColor:colors.green_1st,
    //     borderRadius:RFValue(10),
    //     marginTop:RFValue(-85),
    //     marginLeft:RFValue(45),
    //     justifyContent:'center',
    //     alignItems:'center'
    // },
    // valueNotif :{
    //     fontSize:RFValue(10),
    //     color:colors.white_1st,
    //     fontFamily: 'Montserrat-Regular',
    // }
});
