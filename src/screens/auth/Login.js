import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    AsyncStorage,
    TouchableOpacity,
    Alert,
    TextInput
} from "react-native";
import {
    Button,
} from "native-base";
import colors from "../../component/styles/colors/index";
import {RFValue} from "react-native-responsive-fontsize";
import size from "../../component/styles/size/index";
import IconMI from "react-native-vector-icons/MaterialIcons";
import Loader from "../../component/loader/loader";

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading : false,
            username:'',
            password:'',
            visPass: true,
            hasil:''
        };
    }

    static navigationOptions = {
        header: null
    };

    render() {
        return (
            <View style={styles.container}>
                <Loader loading={this.state.loading} />
                <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 1, flexDirection: 'column'}}>
                            <View style={{width: '100%', height: '30%', paddingTop:20}}>
                            </View>
                            <View style={{width: '100%', height: '20%', justifyContent:'center', alignItems:'center'}}>
                                <Text style={styles.titleLogin}>Login</Text>
                            </View>
                            <View style={{width: '100%', height: '20%'}}>
                                <View style={styles.card}>
                                    <View style={styles.flex}>
                                        <View
                                            hide={false}
                                            style={styles.icon}>
                                            <IconMI
                                                name='account-circle'
                                                size={RFValue(25)}
                                                color='#c6ced0'
                                            />
                                        </View>
                                        <View style={styles.rowField}>
                                            <TextInput
                                                style={styles.input}
                                                placeholder='Username'
                                                onChangeText={(text) => this.setState({username: text})}
                                                value={this.state.username}
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.card}>
                                    <View style={styles.flex}>
                                        <View
                                            hide={false}
                                            style={styles.icon}>
                                            <IconMI
                                                name='lock'
                                                size={RFValue(25)}
                                                color='#c6ced0'
                                            />
                                        </View>
                                        <View style={styles.rowFieldPassword}>
                                            <TextInput
                                                style={styles.inputPassword}
                                                placeholder='Password'
                                                secureTextEntry={this.state.visPass? true : false}
                                                onChangeText={(text) => this.setState({password: text})}
                                                value={this.state.password}
                                            />
                                            <TouchableOpacity
                                                transparent
                                                style={{alignItems:'center', justifyContent:'center'}}
                                                onPress={()=>{
                                                    this.setState({
                                                        visPass:!this.state.visPass,
                                                    })
                                                }}
                                            >
                                                <IconMI
                                                    name={this.state.visPass? 'visibility' : 'visibility-off'}
                                                    size={RFValue(25)}
                                                    color='#c6ced0'
                                                    style={{marginLeft:RFValue(0)}}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={{width: '100%', height: '25%'}}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <View style={{width: '20%', height: '100%'}}>
                                    </View>
                                    <View style={{width: '60%', height: '100%'}}>
                                        <Button
                                            block
                                            style={{
                                                width:'100%',
                                                height: RFValue(35),
                                                marginBottom: 0,
                                                borderWidth: 0,
                                                backgroundColor: colors.green_1st,
                                                borderRadius: 20,
                                                marginTop:RFValue(50)
                                            }}
                                            onPress={() => this.props.navigation.navigate("Dashboard")}
                                        >
                                            <Text style={styles.btnSign}>Login</Text>
                                        </Button>
                                    </View>
                                    <View style={{width: '20%', height: '100%', paddingLeft:40}}/>
                                </View>
                            </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.blue_1st,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    iconTop: {
        width:RFValue(190),
        height:RFValue(190),
        resizeMode:"stretch",
        marginTop: RFValue(25)
    },
    titleLogin: {
        fontSize:RFValue(30),
        color:colors.blue_2st,
        fontFamily: 'Montserrat-Regular',
    },
    signIn: {
        fontSize: size.small_04,
        color:colors.gray_1st,
        marginLeft:RFValue(10),
        fontFamily: 'Montserrat-Regular',
        fontWeight:'bold',
    },
    forgot:{
        fontSize:size.small_03,
        color:colors.gray_1st,
        textAlign:'right'
    },
    dont:{
        color:colors.gray_1st,
        fontSize:size.small_03,
        textAlign:'center'
    },
    create:{
        color:colors.black_1st,
        fontSize:size.small_03,
        fontWeight:'bold'
    },
    btnSign:{
        color:colors.white_1st,
        fontSize:size.small_06,
        fontFamily: 'Montserrat-Regular',
    },
    icon:{
        justifyContent: 'center',
        marginLeft: RFValue(10),
        width:"10%",
    },
    loginButton: {
        marginBottom: 20,
    },
    flex:{
        flex:1,
        flexDirection:'row'
    },
    card:{
        marginLeft: RFValue(40),
        marginRight: RFValue(40),
        borderRadius: 50,
        marginTop:RFValue(18),
        backgroundColor:colors.white_1st,
        height:RFValue(35),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 5,
    },
    rowField: {
        flex: 1,
        marginLeft: 0,
        justifyContent:'center'
    },
    rowFieldPassword: {
        flex: 1,
        flexDirection:'row',
        width:'100%',
        marginLeft: 0,
    },
    input: {
        fontSize:RFValue(14),
        color:colors.gray,
        marginLeft:RFValue(5),
        marginRight: 15,
        fontFamily: 'Montserrat-Regular',
    },
    inputPassword: {
        fontSize:RFValue(14),
        color:colors.gray,
        marginLeft:RFValue(5),
        marginRight: 15,
        width:'80%',
        fontFamily: 'Montserrat-Regular',
    }
});
