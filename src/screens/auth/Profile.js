import React, {Component} from 'react'
import {
    AsyncStorage,
    Alert,
    StyleSheet,
    Text,
    ScrollView,
    TextInput,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    Button,
    Dimensions,
    ImageBackground,
    Platform,
    BackHandler
} from 'react-native'
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
} from "react-native-popup-dialog";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AntDesign from 'react-native-vector-icons/AntDesign'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import CustomFooter from "../../component/footer/CustomFooter";
import {
    Container, Icon,
} from "native-base";
import CustomHeader from "../../component/header/CustomHeader";
import colors from "../../component/styles/colors/index";
import {RFValue} from "react-native-responsive-fontsize";
// import size from "../../component/styles/size/index";
import IconMI from "react-native-vector-icons/MaterialIcons";
// import Loader from "../../component/loader/loader";

export default class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            imageProfile: !null,
            isUploading: false,
            token: '',
            image: '',
            loading: false,
            visible: false,
            profile:[],
            username:[],
        };
    }

    componentDidMount() {
        AsyncStorage.getItem("profile").then(profile => {
            AsyncStorage.getItem("username").then(username => {
                this.setState({
                    profile: JSON.parse(profile),
                    username: JSON.parse(username)
                })
            })
        });
    }

    static navigationOptions = {
        header: null
    };

    updateProfile(){
        this.setState({
            visible: true
        });
    }

    logout(){
        Alert.alert(
            'Confirmation',
            'Logout Tentor Mobile?',
            [
                {
                    text: "No",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "Yes", onPress: () => BackHandler.exitApp() }
            ],
            { cancelable: false }
        );
    }

    editProfile(){

    }


    render(){
        return(
            <Container style={styles.container}>
                <CustomHeader navigation={this.props.navigation} title={'Profile'} left={false}/>
                <ScrollView>
                    <View style={{marginTop:10}}>
                        <View style={{flex:1, flexDirection:'column', marginBottom:RFValue(10)}}>
                            <View style={styles.contentProfile}>
                                <View style={styles.borderProfile}>
                                    <Image style={styles.photoProfile} source={require('../../component/img/avatar.png')}/>
                                </View>
                            </View>
                            <View style={styles.contentName}>
                                <View style={{flex:1, flexDirection:'row'}}>
                                    <Text style={styles.name}>Muhamat Zaenal Mahmut</Text>
                                </View>
                            </View>
                        </View>
                        <TouchableOpacity style={styles.items} onPress={()=>this.props.navigation.navigate('EditProfile')}>
                            <Text style={styles.textLabel}>NIP</Text>
                            <Text style={styles.text}>2103161040</Text>
                        </TouchableOpacity>

                        <View style={styles.items}>
                            <Text style={styles.textLabel}>NAMA</Text>
                            <Text style={styles.text}>Muhamat Zaenal Mahmut</Text>
                        </View>
                        <View style={styles.items}>
                            <Text style={styles.textLabel}>Jabatan</Text>
                            <Text style={styles.text}>Dokter Bedah</Text>
                        </View>
                        <View style={styles.items}>
                            <Text style={styles.textLabel}>Email</Text>
                            <Text style={styles.text}>muhamatzaenalmahmut@gmail.com</Text>
                        </View>
                    </View>
                    <View style={styles.contentLogout}>
                        <View style={styles.btnLogout}>
                            <Text style={styles.textLogout}>Logout</Text>
                        </View>
                    </View>
                </ScrollView>
                <CustomFooter navigation={this.props.navigation} tab="Profile" />
            </Container>

        )
    }
}


const styles = StyleSheet.create({
    header: {
        position: 'absolute',
        height: RFValue(80),
        width: '100%',
        backgroundColor: colors.white_1st,
    },
    contentLogout: {
        height:RFValue(35),
        paddingLeft: RFValue(70),
        paddingRight: RFValue(70),
        marginTop: RFValue(100)
    },
    btnLogout: {
        backgroundColor:colors.green_1st,
        height:'100%',
        borderRadius:RFValue(20),
        justifyContent:'center',
        alignItems:'center'
    },
    textLogout: {
        fontSize:RFValue(14),
        fontFamily: 'Montserrat-Bold',
        color:colors.white_1st,
    },
    contentProfile: {
        height:RFValue(140),
        justifyContent:'center',
        alignItems:'center'
    },
    contentPhoto: {
        height:RFValue(20),
        justifyContent:'center',
        alignItems:'center'
    },
    borderProfile: {
        width:RFValue(120),
        height:RFValue(120),
        borderWidth:RFValue(3),
        borderColor:colors.gray_1st,
        borderRadius:RFValue(80),
        padding: RFValue(4)
    },
    photoProfile: {
        width:'100%',
        height:'100%',
        borderRadius:RFValue(55),
    },
    contentName: {
        height:RFValue(40),
        justifyContent:'center',
        alignItems:'center',
    },
    name: {
        fontSize:RFValue(14),
        fontFamily: 'Montserrat-Bold',
        color:colors.black_1st,
    },
    address: {
        fontSize:RFValue(13),
        fontFamily: 'Montserrat-Regular',
        color:colors.gray_1st,
        marginTop:RFValue(4)
    },
    headerTop: {
        flexDirection: 'row',
        top: 40,
        alignItems: 'center'
    },
    text: {
        color: colors.gray_1st,
        fontSize:RFValue(15),
        fontFamily: 'Montserrat-Regular',
    },
    photo: {
        backgroundColor: colors.blue_1st,
        height: RFValue(150),
        alignItems: 'center',
        width: wp('100%')
    },
    items: {
        backgroundColor: '#FFFFFF',
        flexDirection: 'column',
        height: RFValue(60),
        justifyContent: 'center',
        paddingLeft: 20,
        borderBottomWidth: 1,
        borderColor: '#EFEFEF'
    },
    textLabel: {
        color: colors.black_1st,
        fontWeight: '600',
        fontSize:RFValue(13),
        fontFamily: 'Montserrat-Regular',
    },
})
