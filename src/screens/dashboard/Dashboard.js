import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    ActivityIndicator,
    AsyncStorage,
    FlatList,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    Alert,
    Platform,
    TextInput,
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Card,
    CardItem,
    Textarea
} from "native-base";
import MaterialCommunityicons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
// import HomeMenu from "../../component/HomeMenu";
// import TopMenu from "../../component/TopMenu";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CustomFooter from "../../component/footer/CustomFooter";

import SubApproval from "../../component/card/SubApproval";
import LinearGradient from 'react-native-linear-gradient';
import IconMI from "react-native-vector-icons/MaterialIcons";
import Loader from "../../component/loader/loader";
import colors from "../../component/styles/colors/index";
import {RFValue} from "react-native-responsive-fontsize";
import size from "../../component/styles/size/index";

var that;


class ListApproval extends React.PureComponent {
    navigateToScreen() {
        // AsyncStorage.setItem("listModule", JSON.stringify(listModule)).then(() => {
        that.props.navigation.navigate("DetailApproval");
        // })
    }

    render() {
        return (
            <View style={{marginLeft:RFValue(10), marginRight:RFValue(10)}}>
                <TouchableOpacity
                    transparent
                    onPress={() => this.navigateToScreen()}>
                    <SubApproval
                        title={this.props.data.title}
                        dokter={this.props.data.dokter}
                        time={this.props.data.time}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listApproval :[
                {
                    'title' : 'Pembatalan Jadwal Operasi Bedah Mata',
                    'dokter' : 'dr suzana',
                    'time' : 30
                },
                {
                    'title' : 'Pembatalan',
                    'dokter' : 'dr suzana',
                    'time' : 30
                },
                {
                    'title' : 'Pembatalan Jadwal Operasi Bedah Mata',
                    'dokter' : 'dr suzana',
                    'time' : 30
                },
                {
                    'title' : 'Pembatalan',
                    'dokter' : 'dr suzana',
                    'time' : 30
                },
                {
                    'title' : 'Pembatalan Jadwal Operasi Bedah Mata',
                    'dokter' : 'dr suzana',
                    'time' : 30
                },
            ]
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {

    }

    approval(){
        this.props.navigation.navigate('Approval');
    }

    _renderApproval = ({ item, index }) => <ListApproval data={item} index={index}/>;

    render() {

        that=this;
        return (
            <Container style={styles.container}>
                <Loader loading={this.state.loading} />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.header}>
                        <View style={{width:'74%'}}>
                            <Text style={styles.name}>Hey Zencode,</Text>
                            <Text style={styles.info}>Selamat datang di portal approval</Text>
                        </View>
                        <View style={{width:'13%', alignItems:'flex-end'}}>
                            <Icon
                                name='notifications-outline'
                                style={{fontSize:RFValue(30), color:colors.white_1st, transform: [{ rotate: '-25deg'}]}}
                            />
                        </View>
                        <View style={{width:'13%', alignItems:'flex-end'}}>
                            <TouchableOpacity onPress={()=> this.navigation('Profile')}>
                                <Image style={styles.profile}
                                   source={this.state.imageProfile ? require ( '../../component/img/avatar.png' )
                                       : require("../../component/img/avatar.png")}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.cardTop}>
                        <View style={{flex:1,flexDirection:'row', paddingHorizontal:10}}>
                            <TouchableOpacity style={styles.menu} onPress={()=> this.approval()}>
                                <LinearGradient colors={['#67D9E4', '#7D9AE6']} style={styles.gradient}>
                                    <IconMI
                                        name='import-contacts'
                                        size={RFValue(25)}
                                        color={colors.white_1st}
                                    />
                                </LinearGradient>
                                <Text numberOfLines={2} style={styles.titleMenu}>Rawat Inap</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.menu} onPress={()=> this.approval()}>
                                <LinearGradient colors={['#67D9E4', '#7D9AE6']} style={styles.gradient}>
                                    <IconMI
                                        name='import-contacts'
                                        size={RFValue(25)}
                                        color={colors.white_1st}
                                    />
                                </LinearGradient>
                                <Text numberOfLines={2} style={styles.titleMenu}>Farmasi</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.menu} onPress={()=> this.approval()}>
                                <LinearGradient colors={['#67D9E4', '#7D9AE6']} style={styles.gradient}>
                                    <IconMI
                                        name='import-contacts'
                                        size={RFValue(25)}
                                        color={colors.white_1st}
                                    />
                                </LinearGradient>
                                <Text numberOfLines={2} style={styles.titleMenu}>SIRS</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.menu} onPress={()=> this.approval()}>
                                <LinearGradient colors={['#67D9E4', '#7D9AE6']} style={styles.gradient}>
                                    <IconMI
                                        name='import-contacts'
                                        size={RFValue(25)}
                                        color={colors.white_1st}
                                    />
                                </LinearGradient>
                                <Text numberOfLines={2} style={styles.titleMenu}>Portal Dokter</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.content}>
                        <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                            <Text style={styles.listApproval}>List Approval</Text>
                            <TouchableOpacity>
                                <Text style={styles.lihatLainnya}>Lihat lainnya</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.approval}>
                        <FlatList
                            data={this.state.listApproval}
                            renderItem={this._renderApproval}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>

                </ScrollView>
                <CustomFooter navigation={this.props.navigation} tab="Dashboard" />
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        backgroundColor: colors.blue_3st,
        width:wp('100%'),
        height: RFValue(190),
        paddingLeft: RFValue(10),
        paddingRight: RFValue(10),
        paddingTop:RFValue(25),
        flexDirection:'row',

    },
    name:{
        fontSize:RFValue(20),
        fontWeight:"bold",
        color:colors.white_1st,
        fontFamily: 'Montserrat-Regular',
    },
    balance:{
        fontSize:RFValue(16),
        color:colors.gray_1st,
        fontFamily: 'Montserrat-Regular',
    },
    rp:{
        fontSize:RFValue(16),
        textAlign:'right',
        fontFamily: 'Montserrat-Regular',
    },
    nominal:{
        fontSize:RFValue(12),
        fontFamily: 'Montserrat-Regular',
    },
    info:{
        fontSize:RFValue(13),
        color:colors.white_1st,
        marginTop:RFValue(10),
        fontFamily: 'Montserrat-Regular',
    },
    saldo:{
        flex:1,
        paddingTop: RFValue(15),
        paddingLeft:RFValue(5),
        paddingRight: RFValue(5),
    },
    container: {
        flex: 1,
        backgroundColor: colors.white_2st
    },
    content:{
        flex:1,
        marginTop:5,
        height:RFValue(50),
    },
    cardTop:{
        marginLeft: RFValue(10),
        marginRight: RFValue(10),
        marginTop:RFValue(-80),
        paddingTop:RFValue(30),
        paddingBottom:RFValue(30),
        borderRadius: 10,
        backgroundColor: colors.white_1st,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,

        elevation: 1,
    },
    cardFitur:{
        flex:1,
        flexDirection:'row',
        marginTop:25,
        marginLeft: RFValue(15),
        marginRight: RFValue(15),
    },
    menu:{
        padding:2,
        flex: 1,
        marginHorizontal:10,
        alignItems: 'center',
        width:'25%',
        justifyContent:'flex-start',
        marginBottom:RFValue(20)
    },
    gradient:{
        height:'100%',
        width:'100%',
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center'
    },
    approval:{
        paddingBottom:RFValue(10)
    },
    icon:{
        width: RFValue(25),
        height: RFValue(25),
    },
    iconTop:{
        width: RFValue(40),
        height: RFValue(40),
    },
    title:{
        marginTop:RFValue(15),
        fontSize: RFValue(11),
        color:colors.gray_1st,
        fontFamily: 'Montserrat-Bold',
    },
    titleMenu:{
        marginTop:RFValue(5),
        fontSize: RFValue(12),
        color:colors.black_1st,
        textAlign:'center',
        fontFamily: 'Montserrat-Regular',
    },
    profile:{
        width: RFValue(30),
        height: RFValue(30),
        borderRadius: RFValue(15),
        borderWidth:1,
        borderColor:colors.white_1st
    },
    listApproval: {
        margin:RFValue(15),
        fontSize:RFValue(13),
        fontFamily: 'Montserrat-Regular',
    },
    lihatLainnya: {
        fontSize:RFValue(13),
        margin:RFValue(15),
        color:colors.blue_2st,
        fontFamily: 'Montserrat-Regular',
    },
    // notifAwal: {
    //     width:RFValue(20),
    //     height:RFValue(20),
    //     backgroundColor:colors.green_1st,
    //     borderRadius:RFValue(10),
    //     marginTop:RFValue(10),
    //     // marginLeft:RFValue(45),
    //     justifyContent:'center',
    //     alignItems:'center'
    // },
    // notif: {
    //     width:RFValue(20),
    //     height:RFValue(20),
    //     backgroundColor:colors.green_1st,
    //     borderRadius:RFValue(10),
    //     marginTop:RFValue(-85),
    //     marginLeft:RFValue(45),
    //     justifyContent:'center',
    //     alignItems:'center'
    // },
    // valueNotif :{
    //     fontSize:RFValue(10),
    //     color:colors.white_1st,
    //     fontFamily: 'Montserrat-Regular',
    // }
});
